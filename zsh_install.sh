#!/bin/bash
echo "================================================================================="
echo "                                       zsh                                       "
echo "================================================================================="
cd ~
mkdir zsh 
cd zsh  # 新建文件夹
echo "---------------------------------------------------------------------------------"
echo "wget -O zsh.tar.xz https://sourceforge.net/projects/zsh/files/latest/download #下载最新版本zsh "
wget -O zsh.tar.xz https://sourceforge.net/projects/zsh/files/latest/download #下载最新版本zsh 
echo "---------------------------------------------------------------------------------"
echo "xz -d zsh.tar.xz  # .tar.xz 文件需要解压两次"
xz -d zsh.tar.xz  # .tar.xz 文件需要解压两次
echo "---------------------------------------------------------------------------------"
echo "tar -xvf zsh.tar"
tar -xvf zsh.tar
cd zsh-5.7.1
echo "---------------------------------------------------------------------------------"
echo "./configure --prefix="$HOME/zsh/zsh-5.7.1"  # 指定路径configure"
./configure --prefix="$HOME/zsh/zsh-5.7.1"  # 指定路径configure
echo "---------------------------------------------------------------------------------"
echo " make "
make 
echo "---------------------------------------------------------------------------------"
echo " make install "
make install  # 安装

echo "================================================================================="
echo "                                 oh my zsh                                       "
echo "================================================================================="
cd ~
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh 
cp ~/.oh-my-zsh/templates/zshrc.zsh-template  ~/.zshrc

echo "#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" >> .bashrc
echo "#                 zsh                                   " >> .bashrc
echo "#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" >> .bashrc
echo "export PATH=$HOME/zsh/zsh-5.7.1/bin:$PATH" >> ~/.bashrc
echo "alias zsh=\"exec $HOME/zsh/zsh-5.7.1/bin/zsh\"" >> ~/.bashrc
. ~/.bashrc 

echo "================================================================================="
echo " ## 1. Exacute ZSH presss command : zsh "
echo " ## 2. remember to copy your setting in .bashrc to .zshrc"
echo "================================================================================="

# -=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=
#### 参考材料
# zsh ： https://blog.csdn.net/qq_33660748/article/details/104949930
# oh-my-zsh :  https://www.jianshu.com/p/e91e3dfbb6fd
# mac iterm theme : https://github.com/kaorutsuki/kraft-paper-iterm2-profile
